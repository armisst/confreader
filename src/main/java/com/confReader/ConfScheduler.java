package com.confReader;

import com.google.common.base.Charsets;
import com.google.common.io.CharSink;
import com.google.common.io.CharSource;
import com.google.common.io.FileWriteMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ConfScheduler {

    @Value("${scan.directory}")
    private String scanDirectory;

    @Value("${processed.files.file.name}")
    private String processedFileName;

    @Scheduled(fixedDelayString = "${batch.rate}")
    public void scheduleFixedRateTask() throws Exception {
        Set<String> processedFiles =getProcessedFiles();
        Set<String> newFiles =new HashSet<>();

        DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(scanDirectory),
                path -> path.toString().endsWith(".xml"));

        for (Path path : stream) {
            if (!Files.isDirectory(path)) {
                BasicFileAttributes attributes=  Files.readAttributes(path, BasicFileAttributes.class);
                if(!processedFiles.contains(path.getFileName().toString()) && attributes.size()< 1073741824l) {
                    newFiles.add(path.getFileName().toString());
                    SAXParserFactory spFactory = SAXParserFactory.newInstance();
                    SAXParser saxParser = spFactory.newSAXParser();
                    MyHandler handler = new MyHandler();
                    saxParser.parse(path.toFile() , handler);
                    System.out.println("File name: "+path.getFileName()+" Root tag name: "+ handler.firstElement);
                }
            }
        }

        saveProcessedFiles(newFiles);
     }

    private Set<String> getProcessedFiles() throws IOException{
        File file = new File(processedFileName);
        if(file.exists()){
            CharSource source = com.google.common.io.Files.asCharSource(file, Charsets.UTF_8);
            String result = source.read();
            return new HashSet(Arrays.asList(result.split(";")));
        }else {
            return new HashSet<>();
        }
    }

    private void saveProcessedFiles(Set<String> newFiles) throws IOException {
        if(!newFiles.isEmpty()) {
            String filesStr = newFiles.stream().map(Object::toString).collect(Collectors.joining(";"));
            File file = new File(processedFileName);
            CharSink chs = com.google.common.io.Files.asCharSink(
                    file, Charsets.UTF_8, FileWriteMode.APPEND);
            chs.write(filesStr+";");
        }
    }


}
