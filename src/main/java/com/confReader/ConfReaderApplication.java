package com.confReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ConfReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfReaderApplication.class, args);
	}

}
